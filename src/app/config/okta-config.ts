export default {

  oidc: {
    clientId: '0oauagkkaSZgWdMHq5d6',
    issuer: 'https://dev-65196349.okta.com/oauth2/default',
    redirectUri: 'http://localhost:4200/login/callback',
    scopes: ['openid', 'profile', 'email']

  }

};
